<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admitions', function (Blueprint $table) {
            $table->id();
            $table->string('sf_name');
            $table->string('sl_name');
            $table->string('su_name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('departments_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admitions');
    }
}
