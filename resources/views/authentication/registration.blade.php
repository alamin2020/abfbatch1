@extends('layouts.frontend.master')
@section('title','Sign Up Form')

@section('content')

<br>
<br>
<br>
<br>

 
<div class="container">
	@include('messages.message')
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
  <h2>Sign Up form</h2>
  <form action="{{url('/signUp')}}" method="POST">
  	{{csrf_field()}}
  	<div class="form-group">
      <label for="fname">First Name:</label>
      <input type="text" class="form-control" id="fname" placeholder="first name" name="first_name">
    </div>
    <div class="form-group">
      <label for="fname">Last Name:</label>
      <input type="text" class="form-control" id="fname" placeholder="last name" name="last_name">
    </div>
     <div class="form-group">
      <label for="fname">Mobile  Number:</label>
      <input type="text" class="form-control" id="fname" placeholder="Mobile" name="mobile">
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
    </div>
       <div class="form-group">
      <label for="pwd">Confirm Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Confirm password" name="confirm_password">
    </div>
 
    <button type="submit" class="btn btn-default">Submit</button>
  </form>

</div></div>
</div>


<br>

@endsection