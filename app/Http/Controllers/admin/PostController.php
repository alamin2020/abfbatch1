<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Image;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::join('categories','posts.category_id','=','categories.id')->select('posts.*','categories.category_name')->paginate(4);
        return view('admin.post.post-view',compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.post.post-create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            // 'image' => 'required'
            // |unique:students
        ]);
        $data = new Post;
        if($request->hasFile('post_image')){
            $image = $request->file('post_image');
            $file_name = time().'.'.$image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(990,554);
            $image_resize->save('images/'.$file_name);
            // $request->image->move('images/',$file_name);
            $data->post_image = $file_name;
        }
        $data->category_id = $request->category_id;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->save();


         //  $student = $request->all();
        //    Students::create($student);
           return back()->with('success','Student Data has been added Successfully .');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $category = Category::all();
        return view('admin.post.post-edit',compact('post','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Post::find($id);
        
        if($request->hasFile('post_image')){
            $image = $request->file('post_image');
            $file_name = time().'.'.$image->getClientOriginalExtension();
            // remove previous image file 
            
            $old_image = $data->post_image;
            if($old_image !=""){
                $path = "images/$old_image";
                unlink($path);
            }
            

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(990,554);
            $image_resize->save('images/'.$file_name);
            // $request->image->move('images/',$file_name);
            $data->post_image = $file_name;
        }
        $data->category_id = $request->category_id;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->save();
        return redirect('/post-view')->with('success','Post Data has been updated Successfully .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
