@extends('layouts.frontend.master')
@section('title','Admition - Add Data')
@section('content')
<div class="container">
    <h1>Add Admition User Data :- </h1>
      <div class="">
        @include('messages.message')
          <form action="{{url('admition')}}" method="POST">
              {{-- Token Generate  --}}
             {{ @csrf_field() }}
             
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="first_name">First Name</label>
                  <input type="text" class="form-control" id="first_name" name="sf_name" placeholder="First Name">
                </div>
                <div class="form-group col-md-4">
                  <label for="Last_name">Last Name</label>
                  <input type="text" class="form-control" id="Last_name" name="sl_name" placeholder="Last Name">
                </div>
                <div class="form-group col-md-4">
                  <label for="user_name">User Name</label>
                  <input type="text" class="form-control" id="user_name" name="su_name" placeholder="User Name">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Email</label>
                  <input type="email" class="form-control" id="inputEmail4" name="email" placeholder="Email">
                </div>
                <div class="form-group col-md-4">
                  <label for="inputPassword4">Password</label>
                  <input type="password" class="form-control" id="inputPassword4" name="password" placeholder="Password">
                </div>
                <div class="form-group col-md-4">
                  <label for="dpt_name">Select Your Department </label>
                  <select name="departments_id" id="dpt_name" class="form-control">
                    <option value=""> Select Your Department </option>
                    @foreach ($department_data as $key => $value)
                      <option value=" {{ $value->id}}">{{$value->dpt_name}}</option>
                    @endforeach
                      
                  </select>
                </div>
              </div>
              
             
              <button type="submit" class="btn btn-primary">Save Data</button>
            </form>
      </div>
  </div>
@endsection