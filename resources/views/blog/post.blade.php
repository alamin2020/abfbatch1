@extends('layouts.frontend.master')
@section('title','View blog post page')
@section('content')

<div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="page-title">
            <h2>Blog</h2>
          </div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
  </div>
  <!-- BEGIN Main Container -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <div class="col-left sidebar col-sm-3 blog-side">
          <div class="block widget_search">
            <form id="searchform" action="#" method="get">
              <div class="input-group">
                <input type="text" name="s" id="s" placeholder="Search For" class="input-text" autocomplete="off">
                <div class="input-group-append">
                  <button type="submit" class="thm-search"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
          <div id="secondary" class="widget_wrapper13" role="complementary">
            <div id="recent-posts-4" class="popular-posts widget block" style="visibility: visible;">
              <h2 class="widget-title">Most Popular Posts</h2>
              <ul class="posts-list unstyled clearfix">
                <li>
                  <figure class="featured-thumb"> <a href="#"> <img src="{{asset('frontend/images/blog-img1_1.jpg')}}" alt="blog image">
                    </a> </figure>
                  <!--featured-thumb-->
                  <div class="content-info">
                    <h4>
                        <a href="#" title="Lorem ipsum dolor sit amet">Powerful and flexible premium Ecommercethemes</a>
                    </h4>
                    <p class="post-meta">
                      <time class="entry-date">Jan 10, 2018 .</time>
                      .</p>
                  </div>
                </li>
                <li>
                  <figure class="featured-thumb"> <a href="#"> <img src="images/blog-img3.jpg" alt="blog image"> </a>
                  </figure>
                  <!--featured-thumb-->
                  <div class="content-info">
                    <h4><a href="#" title="Lorem ipsum dolor sit amet">Awesome template with lot's of features on the
                        board!</a></h4>
                    <p class="post-meta">
                      <time class="entry-date">Feb 26, 2018 .</time>
                      .</p>
                  </div>
                </li>
                <li>
                  <figure class="featured-thumb"> <a href="#"> <img src="images/blog-img2.jpg" alt="blog image"> </a>
                  </figure>
                  <!--featured-thumb-->
                  <div class="content-info">
                    <h4><a href="#" title="Lorem ipsum dolor sit amet">Premium template with unlimited colours, and
                        fully Customizable</a></h4>
                    <p class="post-meta">
                      <time class="entry-date">Mar 14, 2018 .</time>
                      .</p>
                  </div>
                </li>
                <li>
                  <figure class="featured-thumb"> <a href="#"> <img src="images/blog-img4.jpg" alt="blog image"> </a>
                  </figure>
                  <!--featured-thumb-->
                  <div class="content-info">
                    <h4><a href="#" title="Lorem ipsum dolor sit amet">
                        Powerful and flexible premium Ecommerce themes</a></h4>
                    <p class="post-meta">
                      <time class="entry-date">Apr 18, 2018 .</time>
                      .</p>
                  </div>
                </li>
              </ul>

              <!--widget-content-->
            </div>
            <div id="categories-2" class="widget widget_categories block" style="visibility: visible;">
              <h2 class="widget-title">Categories</h2>

              <ul>
                @foreach($category as $data)
                <li><a href="#">{{$data->category_name}}</a> <span class="count"></span></li>
                
                @endforeach
              </ul>

            </div>
            <!-- Banner Ad Block -->
            <div class="custom-slider">
              <div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li class="active" data-target="#carousel-example-generic" data-slide-to="0"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="item active"><img src="images/slide3.jpg" alt="slide3">
                      <div class="carousel-caption">
                        <h3><a title=" Sample Product" href="product-detail.html">50% OFF</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a class="link" href="#">Buy Now</a>
                      </div>
                    </div>
                    <div class="item"><img src="images/slide1.jpg" alt="slide1">
                      <div class="carousel-caption">
                        <h3><a title=" Sample Product" href="product-detail.html">Hot collection</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </div>
                    <div class="item"><img src="images/slide2.jpg" alt="slide2">
                      <div class="carousel-caption">
                        <h3><a title=" Sample Product" href="product-detail.html">Summer collection</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </div>
                  </div>
                  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span
                      class="sr-only">Previous</span> </a> <a class="right carousel-control"
                    href="#carousel-example-generic" data-slide="next"> <span class="sr-only">Next</span> </a>
                </div>
              </div>
            </div>
            <!-- Banner Text Block -->

            <div class="block widget_recent_entries">
              <h2 class="widget-title">Recent Posts</h2>
              <ul>
                <li>
                  <a href="#">Post with gallery</a>
                </li>
                <li>
                  <a href="#">Premium template with unlimited colours, and fully Customizable</a>
                </li>
                <li>
                  <a href="#">Post with youtube video</a>
                </li>
                <li>
                  <a href="#">Awesome template with lot’s of features on the board!</a>
                </li>
                <li>
                  <a href="#">Powerful and flexible premium Ecommerce themes</a>
                </li>
              </ul>
            </div>

           

          </div>

        </div>
        <div class="col-main col-sm-9 main-blog">
          <div id="main" class="blog-wrapper">
            <div id="primary" class="site-content">
              <div id="content" role="main">
                  @foreach($post as $value)
                <article id="post-29" class="blog_entry clearfix wow bounceInUp animated animated animated"
                  style="visibility: visible;">

                  <div class="entry-content">
                    <div class="featured-thumb"><a href="#"><img src="{{asset('images/'.$value->post_image)}}" alt="blog-img3"></a></div>
                    <header class="blog_entry-header clearfix">
                      <div class="blog_entry-header-inner">
                        <h2 class="blog_entry-title">
                             <a href="blog-detail.html" rel="bookmark">{{$value->title}}</a>
                        </h2>

                      </div>
                      <!--blog_entry-header-inner-->
                    </header>
                    <div class="entry-content">
                      <ul class="post-meta">
                        <li><i class="fa fa-user"></i>posted by <a href="#">admin</a> </li>
                        <li><i class="fa fa-comments"></i><a href="#">8 comments</a> </li>
                        <li><i class="fa fa-clock-o"></i><span class="day">12</span> <span class="month">Feb</span>
                        </li>
                      </ul>
                      <p>{{$value->description}}</p>
                    </div>
                    <div class="thm-readmore"><a href="#" class="btn">READ MORE</a></div>
                  </div>
                </article>
                @endforeach
                
              </div>
            </div>
            <div class="pager">
              <div class="pages">
                <!--<strong></strong>-->
                <div class="">
                    {{$post->links()}}
                </div>
              </div>
            </div>
          </div>
          <!--#main wrapper grid_8-->

        </div>
        <!--col-main col-sm-9-->
      </div>
    </div>
    <!--main-container-->

  </div>
  <!--col2-layout-->




  <!-- For version 1,2,3,4,6 -->
@endsection