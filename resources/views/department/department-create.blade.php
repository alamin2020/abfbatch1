@extends('layouts.frontend.master')
@section('title','Add Department Data')
@section('content')
<div class="container">
  <h1>Add User Data :- </h1>
    <div class="">
        <form action="{{url('departments-store')}}" method="POST">
            {{-- Token Generate  --}}
            @csrf 
            @if ($errors->any())
              <div class="alert alert-danger">
                  <div>
                      @foreach ($errors->all() as $error)
                          <div>{{ $error }}</div>
                      @endforeach
                  </div>
              </div>
            @endif
            <!-- success massage  -->
            @if(session()->has('success'))
            <div class="alert alert-success" style="color:green;font-weight:bold;">
                {{ session()->get('success') }}
            </div>
            @endif
            <div class="form-row">
             
              <div class="form-group col-md-4">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="f_name" placeholder="First Name">
              </div>
              <div class="form-group col-md-4">
                <label for="Last_name">Last Name</label>
                <input type="text" class="form-control" id="Last_name" name="l_name" placeholder="Last Name">
              </div>
              <div class="form-group col-md-4">
                <label for="user_name">User Name</label>
                <input type="text" class="form-control" id="user_name" name="u_name" placeholder="User Name">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" name="email" placeholder="Email">
              </div>
              <div class="form-group col-md-3">
                <label for="inputPassword4">Password</label>
                <input type="password" class="form-control" id="inputPassword4" name="password" placeholder="Password">
              </div>
              <div class="form-group col-md-3">
                <label for="user_status">User Status</label>
                <select name="user_status" id="user_status" class="form-control">
                    <option value="0">User</option>
                    <option value="1">Admin</option>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="departments">Department</label>
                <input type="text" class="form-control" id="departments" name="dpt_name" placeholder="Department Name">
              </div>
            </div>
            
           
            <button type="submit" class="btn btn-primary">Save Data</button>
          </form>
    </div>
</div>


@endsection