<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;
use Image;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = Students::all();
        return view('student.student-view',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
            'number' => 'required',
            // 'image' => 'required'
            // |unique:students
        ]);
        $data = new Students;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = time().'.'.$image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(300,300);
            $image_resize->save('images/'.$file_name);
            // $request->image->move('images/',$file_name);
            $data->image = $file_name;
        }
        $data->f_name = $request->f_name;
        $data->l_name = $request->l_name;
        $data->number = $request->number;
        $data->save();


         //  $student = $request->all();
        //    Students::create($student);
           return back()->with('success','Student Data has been added Successfully .');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Students::find($id);
        return view('student.profile',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student_data = Students::find($id);
        return view('student.student-edit',compact('student_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Students::find($id);
        
        if($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = time().'.'.$image->getClientOriginalExtension();
            // remove previous image file 
            
            $old_image = $data->image;
            if($old_image !=""){
                $path = "images/$old_image";
                unlink($path);
            }
            

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(300,300);
            $image_resize->save('images/'.$file_name);
            // $request->image->move('images/',$file_name);
            $data->image = $file_name;
        }
        $data->f_name = $request->f_name;
        $data->l_name = $request->l_name;
        $data->number = $request->number;
        $data->save();
        return back()->with('success','Student Data has been updated Successfully .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $data = Students::find($id);
       $old_image = $data->image;
       if($old_image !=""){
           $path = "images/$old_image";
           unlink($path);
       }
        $data->delete();
       return back()->with('success','Student Data Has Successfully Deleted ! ');
    }
}
