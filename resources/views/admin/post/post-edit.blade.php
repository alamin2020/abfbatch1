@extends('layouts.frontend.master')
@section('title','Add Post ...')
@section('content')
<section class="cat-sec" style="padding:30px 0px;"> 
<div class="container pt-4 pb-4">
    <div class="row pb-5">
       
        <div class="col-md-3"></div>
        <div class="col-md-6"> 
            @include('messages.message')
            <h2 >Add Post : </h2>
    <form action="{{url('post/'.$post->id)}}" method='POST' enctype="multipart/form-data" >
        @csrf
        <div class="form-group">
            <label for="category" >Select Category </label>
            <select name="category_id" class="form-control" id="category">
            @foreach($category as $data)
                @if($post->category_id==$data->id)
                    <option value="{{$data->id}}" selected >{{$data->category_name}} </option>
                @else
                    <option value="{{$data->id}}" >{{$data->category_name}} </option>
                @endif
            
            @endforeach
            </select>
            
        </div>
        <div class="form-group">
            <label for="title" > Post Title</label>
            <input type="text" id="title" class="form-control" name="title" value="{{$post->title}}">
        </div>
        <div class="form-group">
            <label for="description" >Post Description</label>
            <textarea name="description" id="description" class="form-control" cols="35" rows="7" value="">{{$post->description}}</textarea>
        </div>
        <div class="form-group">
            <label for="category" >Post Image</label>
            <input type="file" class="form-control" name="post_image" >
            <div class="image">
                <img src="{{asset('images/'.$post->post_image)}}" style="width:70px" alt="">
            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="Update Post">
    </form>
     </div>
    </div>
</div>
</section>
@endsection
