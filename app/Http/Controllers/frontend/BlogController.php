<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
class BlogController extends Controller
{
     public function view(){

        $blogs = Post::orderBy('id','DESC')->paginate(4);

        $categories = Category::with('posts')->get();

   //  return view('categories.index', compact('categories'));
        return view('frontend.blog.view',compact('blogs','categories'));
        
     }
     public function details($id){
        $data = Post::find($id);
        $data->view = $data->view+1 ;
        $data->save();
         return view('frontend.blog.details',compact('data'));
     }
    
}
