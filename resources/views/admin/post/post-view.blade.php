@extends('layouts.frontend.master')
@section('title',' View Post Data')
@section('content')
 
<div class="container" style="padding:30px 0px;">
    @include('messages.message')
    <div class="row">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Sl No </th>
                    <th>Category Name</th>
                    <th>Title </th>
                    <th>Description </th>
                    <th>Photo</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($post as $key => $value )
                <tr>
                    <td>{{$value->id }}</td>
                    <td>{{$value->category_name}}</td>
                    <td>{{$value->title}}</td>
                    <td>{{ substr($value->description,0,40)}}</td>
                    <td><img style="width:70px" src="{{asset('images/'.$value->post_image)}}" alt=""></td>
                    <td> 
                        <a href="{{url('post-add')}}" class="btn btn-success"> Add </a>
                        <a href="{{url('post/'.$value->id.'/edit')}}" class="btn btn-primary"> Edit </a>
                        <a href="{{url('post/'.$value->id.'/delete')}}" class="btn btn-danger" onclick="return confirm('Are You Sure Want to Delete This Data????')"> Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
        <div class="m-auto d-flex text-center">{{$post->links()}}</div>
    </div>
</div>

@endsection
