@extends('layouts.frontend.master')
@section('title','Add Post ...')
@section('content')
<section class="cat-sec" style="padding:30px 0px;"> 
<div class="container pt-4 pb-4">
    <div class="row pb-5">
       
        <div class="col-md-3"></div>
        <div class="col-md-6"> 
            @include('messages.message')
            <h2 >Add Post : </h2>
    <form action="{{url('post/')}}" method='POST' enctype="multipart/form-data" >
        @csrf
        <div class="form-group">
            <label for="category" >Select Category </label>
            <select name="category_id" class="form-control" id="category">
                <option value="">Select Your Category </option>
               
                @foreach($category as $data)
                <option value="{{$data->id}}" >{{$data->category_name}} </option>
                @endforeach
            </select>
            
        </div>
        <div class="form-group">
            <label for="title" > Post Title</label>
            <input type="text" id="title" class="form-control" name="title" placeholder="Post Title ">
        </div>
        <div class="form-group">
            <label for="description" >Post Description</label>
            <textarea name="description" id="description" class="form-control" cols="35" rows="7" placeholder="Write Your Opinion for describetion Your Post ......"></textarea>
        </div>
        <div class="form-group">
            <label for="category" >Post Image</label>
            <input type="file" class="form-control" name="post_image" >
        </div>
        <input type="submit" class="btn btn-primary" value="Save Category">
    </form>
     </div>
    </div>
</div>
</section>
@endsection
