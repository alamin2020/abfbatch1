<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public function role_users()
    {
        return $this->hasOne(Role_users::class);
    }
}
