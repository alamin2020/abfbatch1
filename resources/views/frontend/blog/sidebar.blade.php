<div class="col-left sidebar col-sm-3 blog-side">
          <div class="block widget_search">
            <form id="searchform" action="#" method="get">
              <div class="input-group">
                <input type="text" name="s" id="s" placeholder="Search For" class="input-text" autocomplete="off">
                <div class="input-group-append">
                  <button type="submit" class="thm-search"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
          <div id="secondary" class="widget_wrapper13" role="complementary">
            <div id="recent-posts-4" class="popular-posts widget block" style="visibility: visible;">
              <h2 class="widget-title">Most Popular Posts</h2>
              <ul class="posts-list unstyled clearfix">

                {{-- Foreach loop for popular post  --}}

                @foreach(App\Post::orderBy('view','desc')->get()->take(4) as $ppost)
                <li>
                  <figure class="featured-thumb"> <a href="{{url('blogdetailes/'.$ppost->id)}}"> <img src="{{asset('images/'.$ppost->post_image)}}" alt="blog image">
                    </a> </figure>
                  <!--featured-thumb-->
                  <div class="content-info">
                    <h4>
                        <a href="{{url('blogdetailes/'.$ppost->id)}}" title="Lorem ipsum dolor sit amet">{{$ppost->title}}</a>
                    </h4>
                    <p class="post-meta">
                      <time class="entry-date">{{$ppost->created_at}}</time>
                      .</p>
                  </div>
                </li>
                @endforeach
                {{-- End foreach loop For Popular Post  --}}
              </ul>

              <!--widget-content-->
            </div>
            <div id="categories-2" class="widget widget_categories block" style="visibility: visible;">
              <h2 class="widget-title">Categories</h2>

              <ul>
                {{-- Foreach Loop For Category  --}}
                @foreach(App\Category::all() as $data)

                <li><a href="#">{{$data->category_name}}</a> <span class="count">{{ $data->posts->count() }}</span></li>
                
                {{-- {{ $data->posts->count() }} --}}
                @endforeach

              {{-- End Foreach Loop of Category  --}}
              </ul>

            </div>
            <!-- Banner Ad Block -->
            <div class="custom-slider">
              <div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li class="active" data-target="#carousel-example-generic" data-slide-to="0"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="item active"><img src="{{asset('frontend/images/slide3.jpg')}}" alt="slide3">
                      <div class="carousel-caption">
                        <h3><a title=" Sample Product" href="product-detail.html">50% OFF</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a class="link" href="#">Buy Now</a>
                      </div>
                    </div>
                    <div class="item"><img src="{{asset('frontend/images/slide1.jpg')}}" alt="slide1">
                      <div class="carousel-caption">
                        <h3><a title=" Sample Product" href="product-detail.html">Hot collection</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </div>
                    <div class="item"><img src="{{asset('frontend/images/slide2.jpg')}}" alt="slide2">
                      <div class="carousel-caption">
                        <h3><a title=" Sample Product" href="product-detail.html">Summer collection</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </div>
                  </div>
                  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span
                      class="sr-only">Previous</span> </a> <a class="right carousel-control"
                    href="#carousel-example-generic" data-slide="next"> <span class="sr-only">Next</span> </a>
                </div>
              </div>
            </div>
            <!-- Banner Text Block -->

            <div class="block widget_recent_entries">
              <h2 class="widget-title">Recent Posts</h2>
              <ul>

                {{-- Foreach Loop for Recent post  --}}

                @foreach(App\Post::orderBy('id','desc')->get()->take(4) as $rpost)
                <li>
                  <a href="{{url('blogdetailes/'.$rpost->id)}}">{{$rpost->title}}</a>
                </li>
               @endforeach
               {{-- End foreach loop of Recent post --}}
              </ul>
            </div>

           

          </div>

        </div>