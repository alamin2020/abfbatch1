<?php

use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('frontend.index');
});
// Authenticate Route For Login
Route::get('sign-in','LoginController@loginForm');
Route::get('logout','LoginController@logout');
Route::post('loginUser','LoginController@login');

// Authenticate Route For Register
Route::get('sign-up','RegistrationController@create');
Route::post('signUp','RegistrationController@register');
// 


Route::get('about-us','homeController@about')->name('about');
Route::get('contact-us','homeController@contact')->name('contact');
Route::get('service-us','homeController@service')->name('service');
// Data Add from Database---</>
Route::get('departments/create','departmentController@create');
Route::post('departments-store','departmentController@departmentsStore');
// Data view from Database ---</>
Route::get('department-view','departmentController@index')->name('departments/view');
Route::get('departments/delete/{id}','departmentController@destroy');
Route::get('departments/{id}/edit','departmentController@edit');
Route::post('departments/{id}','departmentController@update');

 // Admition controller are here ---</>
 Route::get('admition/create','AdmitionController@create')->name('admition/create');
 Route::get('admition/view','AdmitionController@index')->name('admition-view');
 Route::post('admition','AdmitionController@store');
 Route::get('admition/delete/{id}','AdmitionController@destroy');
 Route::delete('admition/{id}','AdmitionController@destroy');
 Route::get('admition/{id}/edit','AdmitionController@edit');
 Route::patch('admition/{id}','AdmitionController@update');

// Student Controller Are Here ---</>
Route::get('student/create','StudentController@create')->name('student/create');
Route::get('student/view','StudentController@index')->name('student-view');

Route::post('student','StudentController@store');
Route::delete('student/{id}','StudentController@destroy');
Route::get('student/{id}/edit','StudentController@edit');
Route::patch('student/{id}','StudentController@update');
Route::get('student/{id}/view','StudentController@show');

// Category Routs

Route::get('category-add','admin\CategoryController@create');
Route::post('category','admin\CategoryController@store');
Route::post('category/{id}','admin\CategoryController@update');
Route::get('category-view','admin\CategoryController@index');
Route::get('category/{id}/edit','admin\CategoryController@edit');
Route::get('category/{id}/delete','admin\CategoryController@destroy');

// Post Routs 

Route::get('post-add','admin\PostController@create');
Route::post('post','admin\PostController@store');
Route::get('post-view','admin\Postcontroller@index');
Route::get('post/{id}/edit','admin\PostController@edit');
Route::post('post/{id}','admin\PostController@update');

// Frontend Routs
// Route::get('blog-post','frontend\PostController@index');
Route::get('blogs','Frontend\BlogController@view');
Route::get('blogdetailes/{id}','Frontend\BlogController@details');

// Comments
Route::post('comment','frontend\CommentController@store');

