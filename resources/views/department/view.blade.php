<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> DEpartment View   </h1>
    <h3><a href="/departments/create" > Add New Department </a></h3>
    <!-- error messege -->
    @if($errors->any())
    @foreach ($errors->all() as $error)
        <div style="color:red;">{{ $error }}</div>
    @endforeach
    @endif

    <!-- success massage  -->
    @if(session()->has('success'))
    <div class="alert alert-success" style="color:green;font-weight:bold;">
        {{ session()->get('success') }}
    </div>
    @endif

    <!-- <form action="/departments" method="POST">
    token laravel
    {{csrf_field()}}
        <input type="text" name="dpt_name" placeholder="department Name">
        <input type="text" name="dpt_code" placeholder="Department code">
        <input type="submit" value="SAVE">
    </form>
    <br>
    <hr> -->
    <h1> Show Data From Database </h1>
    <table border="1" style="border-collapse:collapse;">
        <tr>
            <th>Sl No.</th>
            <th>Dpt Name </th>
            <th>Dpt Code</th>
            <th>Action </th>
        </tr>
        <!-- Show Data Dynamically -->
        @foreach($departments as $key=>$value)
        <tr>
            <td>{{++$key}}</td>
            <td>{{$value->dpt_name}}</td>
            <td>{{$value->dpt_code}}</td>
            <td><a href="#">Edit </a> || <a href="/department/delete/{{$value->id}}">Delete</a></td>
        </tr>
        @endforeach
        
    </table>
    <!-- {{$departments->links()}} -->
</body>
</html>