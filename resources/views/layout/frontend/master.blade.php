@include('layouts.frontend.header')

<div class="pt-5 mt-5">
    @yield('content')
</div>


@include('layouts.frontend.footer')