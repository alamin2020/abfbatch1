@extends('layouts.frontend.master')
@section('title','Add Category Page')
@section('content')
<section class="cat-sec" style="padding:30px 0px;"> 
<div class="container pt-4 pb-4">
    <div class="row pb-5">
       
        <div class="col-md-3"></div>
        <div class="col-md-6"> 
            @include('messages.message')
            <h2 >Add Category : </h2>
    <form action="category" method='POST' >
        @csrf
        <div class="form-group">
            <label for="category" >Category Name</label>
            <input type="text" class="form-control" name="category_name" placeholder="Category Name ">
        </div>
        <input type="submit" class="btn btn-primary" value="Save Category">
    </form>
     </div>
    </div>
</div>
</section>
@endsection
