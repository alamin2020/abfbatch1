<!-- error messege -->
    @if($errors->any())
    @foreach ($errors->all() as $error)
        <div style="color:red;">{{ $error }}</div>
    @endforeach
    @endif

    <!-- success massage  -->
    @if(session()->has('success'))
    <div class="alert alert-success" style="color:green;font-weight:bold;">
        {{ session()->get('success') }}
    </div>
    @endif

    <!-- warning massage  -->
    @if(session()->has('warning'))
    <div class="alert alert-warning" style="color:red;font-weight:bold;">
        {{ session()->get('warning') }}
    </div>
    @endif