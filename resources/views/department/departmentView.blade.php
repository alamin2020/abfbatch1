@extends('layouts.frontend.master')
@section('title','Department Data View')
@section('content')
  {{-- Errow message  --}}
  @if ($errors->any())
  <div class="alert alert-danger">
      <div>
          @foreach ($errors->all() as $error)
              <div>{{ $error }}</div>
          @endforeach
      </div>
  </div>
@endif
<!-- success massage  -->
@if(session()->has('success'))
<div class="alert alert-success" style="color:green;font-weight:bold;">
    {{ session()->get('success') }}
</div>
@endif

<div class="">
    <h1> This is view page</h1>
    <table class="table table-bordered table-hover ta">
        <thead class="thead-light">
            <tr>
                <td>Sl No</td>
                <td>Id No</td>
                <td>First Name</td>
                <td>Last Name </td>
                <td>User Name</td>
                <td style="width:250px !important ">Email</td>
                <td>Department Name </td>
                <td>User Status</td>
                <td style="width:350px">action</td>
            </tr>
        </thead>
       
        <tbody> 
            <?php
            // code per page pagination ---for all page
                $i = $data->perPage()*($data->currentPage()-1);    
            ?>
            @foreach ($data as $key=>$row)
            <tr>
                <td>{{++$i}}</td>
                <td>{{$row->id}}</td>
                <td>{{$row->f_name}}</td>
                <td>{{$row->l_name}}</td>
                <td>{{$row->u_name}}</td>
                <td style="width:250px">{{$row->email}}</td>
                <td>{{$row->dpt_name}}</td>
                
                <td>
                    @if ( $row->user_status == '1')
                    <a class="btn btn-warning btn-sm btn-mini p-2" role="button" href="">active</a>
                    @else
                    <a class="btn btn-primary p-2" role="button" href="">inactive</a> 
                    @endif
                </td>
                <td style="width:350px">
                <a class="btn btn-primary p-2" href="{{url('departments/'.$row->id.'/edit')}}">Edit</a>
                    <a class="btn btn-danger p-2" href="{{url('departments/delete/'.$row->id)}}" onclick="return confirm('are you sure delete this data')">Delete</a>
                    <a class="btn btn-success p-2" href="{{url('departments/create')}}">Add</a>
                </td>
                {{-- {!! Form::open(['url' => 'foo/bar']) !!}
                     //
                {!! Form::close() !!} --}}
            </tr>
            @endforeach
        </tbody>
    </table>
    {{-- Code for pagination  --}}
    <div class="container">
        <div class="m-auto d-flex justify-content-center">{{$data->links()}}</div>
    </div>
    
    
</div>
    
@endsection
