@extends('layouts.frontend.master')

@section('title','Update Student Data..')
@section('content')
  <div class="container">
    <h2>Student Id : 1120296 </h2>
    <div class="">
        @include('messages.message')

        
    {!! Form::open(['url' => 'student/'.$student_data->id, 'method'=>'PATCH' , 'class'=>'form-group', 'enctype'=>'multipart/form-data']) !!}
   
             
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="f_name" value="{{$student_data->f_name}}">
                </div>
                <div class="form-group col-md-6">
                  <label for="Last_name">Last Name</label>
                  <input type="text" class="form-control" id="Last_name" name="l_name" value="{{$student_data->l_name}}">
                </div>
                <div class="form-group col-md-4">
                  <label for="user_name">Phone Number </label>
                  <input type="number" class="form-control" id="user_name" name="number" value="{{$student_data->number}}" >
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Image Name </label>
                  <input type="file" class="form-control" id="inputEmail4" name="image" >

                </div>
                <div class="col-md-4">
                <img  class="img-circle" src="{{asset('images/'.$student_data->image)}}" alt="" height="100px">
                </div>
              </div>
              
             
              <button type="submit" class="btn btn-primary">Update Data</button>
           {!! form::close() !!}
      </div>
  </div>



    @endsection

