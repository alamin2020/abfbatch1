<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    protected $fillable = ['f_name','l_name','u_name','email','password','user_status','dpt_name'];
    
}
