@extends('layouts.frontend.master')

@section('title','Update Admition Data..')
@section('content')
  <div class="container">
    <h2>Add Student</h2>
    <div class="">
        @include('messages.message')
          <form action="{{url('student')}}" method="POST" enctype="multipart/form-data">
              {{-- Token Generate  --}}
             {{ @csrf_field() }}
             
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="first_name">First Name</label>
                  <input type="text" class="form-control" id="first_name" name="f_name" placeholder="First Name">
                </div>
                <div class="form-group col-md-4">
                  <label for="Last_name">Last Name</label>
                  <input type="text" class="form-control" id="Last_name" name="l_name" placeholder="Last Name">
                </div>
                <div class="form-group col-md-4">
                  <label for="user_name">Phone Number </label>
                  <input type="number" class="form-control" id="user_name" name="number" placeholder="Phone Number">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Image Name </label>
                  <input type="file" class="form-control" id="inputEmail4" name="image" >
                </div>
              </div>
              
             
              <button type="submit" class="btn btn-primary">Save Data</button>
            </form>
      </div>
  </div>
    @endsection

