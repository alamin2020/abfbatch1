@extends('layouts.frontend.master')
@section('title','Category View Page')
@section('content')
 
<div class="container" style="padding:30px 0px;">
    @include('messages.message')
    <div class="row">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Sl No </th>
                    <th>Category Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $key => $value )
                <tr>
                    <td>{{$value->id }}</td>
                    <td>{{$value->category_name}}</td>
                    <td> 
                        <a href="{{url('category-add')}}" class="btn btn-success"> Add </a>
                        <a href="{{url('category/'.$value->id.'/edit')}}" class="btn btn-primary"> Edit </a>
                        <a href="{{url('category/'.$value->id.'/delete')}}" class="btn btn-danger" onclick="return confirm('Are You Sure Want to Delete This Data????')"> Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
        <div class="m-auto d-flex text-center">{{$data->links()}}</div>
    </div>
</div>

@endsection
