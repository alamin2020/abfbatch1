@extends('layouts.frontend.master')
@section('title','Detail :'.$data->title)
@section('content')


<div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="page-title">
            <h2>Blog</h2>
          </div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
  </div>
  <!-- BEGIN Main Container -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        
 @include('frontend.blog.sidebar')


        <div class="col-main col-sm-9 main-blog">
          <div id="main" class="blog-wrapper">
            <div id="primary" class="site-content">
              <div id="content" role="main">
                    {{--  view post every dynamically --}}
                 
                <article id="post-29" class="blog_entry clearfix wow bounceInUp animated animated animated"
                  style="visibility: visible;">

                  <div class="entry-content">
                    <div class="featured-thumb"><a href="{{url('blogdetailes/'.$data->id)}}"><img src="{{asset('images/'.$data->post_image)}}" alt="blog-img3"></a></div>
                    <header class="blog_entry-header clearfix">
                      <div class="blog_entry-header-inner">
                        <h2 class="blog_entry-title">
                             <a href="{{url('blogdetailes/'.$data->id)}}" rel="bookmark">{{$data->title}}</a>
                        </h2>

                      </div>
                      <!--blog_entry-header-inner-->
                    </header>
                    <div class="entry-content">
                      <ul class="post-meta">
                        <li><i class="fa fa-user"></i>posted by <a href="#">admin</a> </li>
                        <li><i class="fa fa-comments"></i><a href="#">8 comments</a> </li>
                        <li><i class="fa fa-clock-o"></i><span class="day">12</span> <span class="month">Feb</span>
                        </li>
                      </ul>
                      <p>{{$data->description}}</p>
                    </div>
                  </div>
                </article>


                {{-- Comment section --}}

                <div class="comment-content">
                    <div class="comments-wrapper">
                      <h3> Comments </h3>
                      <ol class="commentlist">
                        {{-- Foreatch Loop --}}
                        @foreach(App\Comments::all() as $comment)
                        <li class="comment">
                          <div>
                            <img alt="" src="images/member1.png" class="avatar avatar-60 photo">
                            <div class="comment-text">
                              <div class="ratings">
                                <div class="rating-box">
                                  <div style="width:100%" class="rating"></div>
                                </div>

                              </div>
                              <p class="meta">
                                <strong>{{$comment->name }}</strong>
                                <span>–</span> <time>{{$comment->created_at }}</time>
                              </p>
                              <div class="description">
                                <p> {{$comment->comment }}</p>
                              </div>
                            </div>
                          </div>
                        </li><!-- #comment-## -->
                        
                        @endforeach
                      </ol>
                      <!--commentlist-->
                    </div>
                    <!--comments-wrapper-->

                    <div class="comments-form-wrapper comment-respond">
                      <h3>Leave A reply</h3>
                      <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span>
                        Required fields are marked <span class="required">*</span></p>
                        {{-- message --}}
                    @include('messages.message')
                        {{-- Add comment form --}}
                      <form class="comment-form" method="post"  action="{{url('comment')}}">
                        {{-- Token Grnerate --}}
                        @csrf 
                        
                        <div class="field">
                          <label for="name">Name<em class="required">*</em></label>
                          <input type="text" class="input-text" title="Name" value="" id="user" name="name"
                            placeholder="Name" required>
                        </div>
                        <div class="field">
                          <label for="email">Email<em class="required">*</em></label>
                          <input type="text" class="input-text validate-email" title="Email" value="" id="email"
                            name="email" placeholder="Email Address" required>
                        </div>
                        <div class="field">
                          <label for="email">Website<em class="required">*</em></label>
                          <input type="text" class="input-text validate-email" title="Email" value="" id="email"
                            name="website" placeholder="Website">
                        </div>
                        <div class="clear"></div>
                        <div class="field aw-blog-comment-area">
                          <label for="comment">Comment<em class="required">*</em></label>
                          <textarea rows="5" cols="50" class="input-text" title="Comment" id="comment" name="comment"
                            placeholder="Comment" required></textarea>
                        </div>
                        <div style="width:96%" class="button-set">
                          <input type="hidden" value="{{$data->id}}" name="posts_id">
                          <button type="submit" class="bnt-comment"><span><span>Add Comment</span></span></button>
                        </div>
                      </form>
                    </div>
                    <!--comments-form-wrapper clearfix-->

                  </div>
                
              </div>
            </div>
            {{-- <div class="pager">
              <div class="pages">
                <!--<strong></strong>-->
                <div class="">
                    
                </div>
              </div>
            </div> --}}
          </div>
          <!--#main wrapper grid_8-->

        </div>
        <!--col-main col-sm-9-->
      </div>
    </div>
    <!--main-container-->

  </div>
  <!--col2-layout-->




  <!-- For version 1,2,3,4,6 -->

@endsection