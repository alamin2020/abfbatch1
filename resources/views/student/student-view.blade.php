@extends('layouts.frontend.master')

@section('title','View Student Profile Data !')

@section('content')
@include('messages.message')
<div class="container"><h1>This Is Student View Page :-</h1></div>
<table class="table table-bordered table-responsive">
    <thead>
        <tr>
            <th>Sl No</th>
            <th>Student Name</th>
            <th>Phone Number</th>
            <th>Photo </th>
            <th>Edit</th>
            <th>Delete</th>
            <th>View</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $value)
        <tr>
            <td>{{ $value->id}}</td>
            <td>{{ $value->f_name .' - '. $value->l_name }}</td>
            <td>{{ $value->number}}</td>
        <td > <img height="70px" src="{{asset('images/'.$value->image)}}" alt=""></td>
            <td>
            <a href="{{url('student/'.$value->id.'/edit')}}" class="btn btn-info p-3">Edit</a>
            </td>
            <td>
            {!! Form::open(['url' => 'student/'.$value->id, 'method'=>'delete']) !!}
                <button class="btn-warning" onclick="return confirm('Are You Sure ?')"> Delete</button>
            {!! Form::close() !!}
            </td>
            <td>
                <a href="{{url('student/'.$value->id.'/view')}}" class="btn btn-success p-3">View Profile</a>
            </td>
        </tr>
        @endforeach
        
    </tbody>
</table>
    
@endsection