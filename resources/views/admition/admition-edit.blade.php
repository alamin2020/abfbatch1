@extends('layouts.frontend.master')

@section('title','Update Admition Data..')
@section('content')
<div class="container">
    <h1>Admition Update</h1><hr>
    @include('messages.message')
   

    {!! Form::open(['url' => 'admition/'.$admition_data->id, 'method'=>'PATCH' , 'class'=>'form-group', 'enctype'=>'multipart/form-data']) !!}
   

  
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="first_name">First Name</label>
          <input type="text" class="form-control" id="first_name" name="sf_name" value="{{ $admition_data->sf_name}}">
          </div>
          <div class="form-group col-md-4">
            <label for="Last_name">Last Name</label>
            <input type="text" class="form-control" id="Last_name" name="sl_name" value="{{ $admition_data->sl_name}}">
          </div>
          <div class="form-group col-md-4">
            <label for="user_name">User Name</label>
            <input type="text" class="form-control" id="user_name" name="su_name" value="{{ $admition_data->su_name}}">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputEmail4">Email</label>
            <input type="email" class="form-control" id="inputEmail4" name="email" value="{{ $admition_data->email}}" >
          </div>
          
          <div class="form-group col-md-4">
            <label for="dpt_name">Select Your Department </label>
            <select name="departments_id" id="dpt_name" class="form-control">
            {{-- <option value="{{$admition_data->departments_id}}">{{$admition_data->departments->dpt_name}}</option> --}}

              @foreach ($department as $key => $value)
                @if($value->id == $admition_data->departments_id)
                  {{-- If we want to select Department --}}
                  <option value="{{$value->id}}" selected>{{$value->dpt_name}}</option>
                @else

                <option value="{{$value->id}}">{{$value->dpt_name}}</option>

                @endif
              @endforeach
                
            </select>
          </div>
        </div>
        
       
        <button type="submit" class="btn btn-primary">Save Data</button>
   
        {!! Form::close() !!}
</div>
    
@endsection