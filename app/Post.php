<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['category_id','title','description','post_image'];
    
    public function categories()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function comments(){
        return $this->hasMany('App\Comments');
    }
}
