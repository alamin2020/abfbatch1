@extends('layouts.frontend.master')
@section('title','Blog Post')
@section('content')


<div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="page-title">
            <h2>Blog</h2>
          </div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
  </div>
  <!-- BEGIN Main Container -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        
 @include('frontend.blog.sidebar')


        <div class="col-main col-sm-9 main-blog">
          <div id="main" class="blog-wrapper">
            <div id="primary" class="site-content">
              <div id="content" role="main">
                    {{--  view post every dynamically --}}
                  @foreach($blogs as $value)
                <article id="post-29" class="blog_entry clearfix wow bounceInUp animated animated animated"
                  style="visibility: visible;">

                  <div class="entry-content">
                    <div class="featured-thumb"><a href="{{url('blogdetailes/'.$value->id)}}"><img src="{{asset('images/'.$value->post_image)}}" alt="blog-img3"></a></div>
                    <header class="blog_entry-header clearfix">
                      <div class="blog_entry-header-inner">
                        <h2 class="blog_entry-title">
                             <a href="{{url('blogdetailes/'.$value->id)}}" rel="bookmark">{{$value->title}}</a>
                        </h2>

                      </div>
                      <!--blog_entry-header-inner-->
                    </header>
                    <div class="entry-content">
                      <ul class="post-meta">
                        <li><i class="fa fa-user"></i>posted by <a href="#">admin</a> </li>
                        <li><i class="fa fa-comments"></i><a href="#">8 comments</a> </li>
                        <li><i class="fa fa-clock-o"></i><span class="day">12</span> <span class="month">Feb</span>
                        </li>
                      </ul>
                      <p>{{substr($value->description,0,300)}}</p>
                    </div>
                    <div class="thm-readmore"><a href="{{url('blogdetailes/'.$value->id)}}" class="btn">READ MORE</a></div>
                  </div>
                </article>
                @endforeach
                
              </div>
            </div>
            <div class="pager">
              <div class="pages">
                <!--<strong></strong>-->
                <div class="">
                    {{$blogs->links()}}
                </div>
              </div>
            </div>
          </div>
          <!--#main wrapper grid_8-->

        </div>
        <!--col-main col-sm-9-->
      </div>
    </div>
    <!--main-container-->

  </div>
  <!--col2-layout-->




  <!-- For version 1,2,3,4,6 -->

@endsection