<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;

class departmentController extends Controller
{

    public function create(){
        return view('department.department-create');
    }
    public function departmentsStore(Request $request){
        $validatedData = $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
            'u_name' => 'required|unique:departments|max:25',
            'email' => 'required|unique:departments|max:25',
            'password' => 'required',
            'user_status' => 'required',
            'dpt_name' => 'required',
        ]);
        $data = $request->all();
        Departments::create($data);
        return back()->with('success','Data Inserted successfully');
    }
    // Code for viewing data from database ---</>
    public function index(){
       $data = Departments::orderBy('id','desc')->paginate(3);
        return view('department.departmentView',compact('data'));
    }
    // Code for delete Data ---</>
    public function destroy($id){
        $data = Departments::find($id);
        $data->delete();
        return back()->with('success','Data Has been Deleted ');
    }
    // Code view edit form --</>
    public function edit($id){
        $data = Departments::find($id);
        return view('department.edit',compact('data'));
    }
    // Code for update ----</>
    public function update(Request $request,$id){
        $data = Departments::find($id);
        $new_data = $request->all();
        $data->update($new_data);
        return redirect('department-view')->with('success','Data Updated successfull');
    }
}
