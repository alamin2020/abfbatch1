<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;
use App\Admitions;

class AdmitionController extends Controller
{
//     public function __construct()
// {
//     $this->middleware('auth');
// }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $admition = Admitions::join('departments','admitions.departments_id','=','departments.id')->select('admitions.*','departments.dpt_name')->paginate(3);
        
        return view('admition.admitiondata-view',compact('admition'));
    //     $department_data = Departments::orderBy('id','desc')->paginate(3);
    //    return $department_data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department_data = Departments::all();
        $admition = Departments::all();
      
       return view('admition.create',compact('department_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'sf_name' => 'required',
            'sl_name' => 'required',
            'su_name' => 'required|unique:admitions|max:25',
            'email' => 'required|unique:admitions|max:25',
            'password' => 'required',
            'departments_id' => 'required',
        ]);
        $admition = $request->all();
        Admitions::create($admition);
        return back()->with('success','Data Inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admition_data = Admitions::find($id);
        $department = Departments::all();
        return view('admition.admition-edit',compact('admition_data','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Admitions::find($id);
        $new_data = $request->all();
        $data->update($new_data);
        return back()->with('success','Student Data Update Successfully ..');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Admitions::find($id);
        $data->delete();
        return back()->with('success','Data Has been Deleted ');
    }
}
