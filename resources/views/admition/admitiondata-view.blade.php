@extends('layouts.frontend.master')
@section('title','Admition Data View')
@section('content')
    <div class="">
    <h1>This is Admition view page</h1>
    @include('messages.message')
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>SL No</th>
                <th>Student Name</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Department Name</th>
                <th>Action</th>
                <th>Delete Method </th>
                
            </tr>
        </thead>
        <tbody>
            <?php
            // code per page pagination ---for all page
                $i = $admition->perPage()*($admition->currentPage()-1);    
            ?>
            @foreach ($admition as $key=>$value)
            <tr>
                <td> {{ ++$i }} </td>
                <td> {{ $value->sf_name .' '.$value->sl_name }} </td>
                <td> {{ $value->su_name }}</td>
                <td> {{ $value->email }}</td>
                <td> {{ $value->dpt_name }}</td>
                <td> 
                    <a href="{{url('admition/'.$value->id.'/edit')}}" class="btn btn-info p-2">Edit</a>
                    <a href="{{url('admition/delete/'.$value->id)}}" class="btn btn-danger p-2" onclick="return confirm('Are You Sure ?')">Delete</a>
                    <a href="{{url('admition/create')}}" class="btn btn-success p-2">Add</a>
                </td>
                <td>
                    {!! Form::open(['url' => 'admition/'.$value->id.'/delete', 'method'=>'delete']) !!}
                    <button class="btn-warning" onclick="return confirm('Are You Sure ?')"> Delete</button>
                    {!! Form::close() !!}
                </td>
                
            </tr>
            
            @endforeach
        </tbody>
    </table>
    <div class="container">
        <div class="m-auto d-flex justify-content-center">{{$admition->links()}}</div>
    </div>
    <a href="" class="btn btn-info ">Update</a>
    
    </div>
@endsection